#ifndef WINGEDEDGETESTSUITE_H
#define WINGEDEDGETESTSUITE_H

#include <string>
#include "WingedEdge.h"

class WingedEdgeTestSuite {
public:
    WingedEdgeTestSuite(const WingedEdge& we): wingedEdge(we) {};
    virtual ~WingedEdgeTestSuite() {};
    
    void runAllTests() const;
    void runVertexTests() const;
    void runFaceTests() const;
    void runEdgeTests() const;
private:
    const WingedEdge& wingedEdge;
};

#endif /* WINGEDEDGETESTSUITE_H */

