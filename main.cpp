#include "WingedEdgeTestSuite.h"

int main() {
    const auto& testFiles = {"tetraeder.json", "pentaeder.json"};
    
    for (const auto& testFile : testFiles) {
        std::cout << "\nRunning tests for file \"" << testFile << "\"" << "...\n" << std::endl;
        WingedEdge wingedEdge(testFile);
        
        WingedEdgeTestSuite testSuite(wingedEdge);
        testSuite.runAllTests();
    }

    return 0;
}