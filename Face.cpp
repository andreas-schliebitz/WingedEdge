#include "Face.h"

Face::Face(const std::string& faceName) {
    this->setFaceName(faceName);
}

const std::string& Face::getFaceName() const {
    return this->faceName;
}

Edge* const Face::getIncidentEdge() const {
    return this->incidentEdge;
}

void Face::setFaceName(const std::string& faceName) {
    this->faceName = faceName;
}

void Face::setIncidentEdge(Edge* incidentEdge) {
    this->incidentEdge = incidentEdge;
}

std::ostream& operator<<(std::ostream& stream, const Face& face) {
    stream << "["
            << "FaceName: " << face.getFaceName()
            << ", IncidentEdge: " << face.getIncidentEdge()->getEdgeName()
            << "]";
    return stream;
}