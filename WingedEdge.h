#ifndef WINGEDEDGE_H
#define WINGEDEDGE_H

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <iostream>

#include "Face.h"
#include "Vertex.h"
#include "Edge.h"

class WingedEdge {
public:
    WingedEdge() {};
    WingedEdge(const std::string& modelPath);

    virtual ~WingedEdge();
    
    const std::unordered_map<std::string, Vertex*>& getVertexTable() const;
    const std::unordered_map<std::string, Edge*>& getEdgeTable() const;
    const std::unordered_map<std::string, Face*>& getFaceTable() const;
    
    std::vector<Vertex*> getVerticesAdjacentToEdge(const std::string& edgeName) const;
    std::vector<Edge*> getEdgesAdjacentToEdge(const std::string& edgeName) const;
    std::vector<Face*> getFacesAdjacentToEdge(const std::string& edgeName) const;
    
    std::vector<Vertex*> getVerticesAdjacentToVertex(const std::string& vertexName) const;
    std::vector<Edge*> getEdgesAdjacentToVertex(const std::string& vertexName) const;
    std::unordered_set<Face*> getFacesAdjacentToVertex(const std::string& vertexName) const;
    
    std::unordered_set<Vertex*> getVerticesAdjacentToFace(const std::string& faceName) const;
    std::vector<Edge*> getEdgesAdjacentToFace(const std::string& faceName) const;
    std::unordered_set<Face*> getFacesAdjacentToFace(const std::string& faceName) const;
    
    template<typename T>
    static void printAdjacencies(const T& adjacencies) {
        for (const auto& a : adjacencies) {
            std::cout << "\t" << *a << std::endl;
        }
        std::cout << std::endl;
    }
private:
    std::unordered_map<std::string, Edge*> edgeTable;
    std::unordered_map<std::string, Vertex*> vertexTable;
    std::unordered_map<std::string, Face*> faceTable;
};

#endif /* WINGEDEDGE_H */