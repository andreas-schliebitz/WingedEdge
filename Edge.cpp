#include "Edge.h"

Edge::Edge(const std::string& edgeName) {
    this->setEdgeName(edgeName);
}

const std::string& Edge::getEdgeName() const {
    return this->edgeName;
}

Vertex* const Edge::getStartVertex() const {
    return this->startVertex;
}

Vertex* const Edge::getEndVertex() const {
    return this->endVertex;
}

Face* const Edge::getLeftFace() const {
    return this->leftFace;
}

Face* const Edge::getRightFace() const {
    return this->rightFace;
}

Edge* const Edge::getLeftPrevEdge() const {
    return this->leftPrevEdge;
}

Edge* const Edge::getLeftNextEdge() const {
    return this->leftNextEdge;
}

Edge* const Edge::getRightPrevEdge() const {
    return this->rightPrevEdge;
}

Edge* const Edge::getRightNextEdge() const {
    return this->rightNextEdge;
}

void Edge::setEdgeName(const std::string& edgeName) {
    this->edgeName = edgeName;
}

void Edge::setStartVertex(Vertex* startVertex) {
    this->startVertex = startVertex;
}

void Edge::setEndVertex(Vertex* endVertex) {
    this->endVertex = endVertex;
}

void Edge::setLeftFace(Face* leftFace) {
    this->leftFace = leftFace;
}

void Edge::setRightFace(Face* rightFace) {
    this->rightFace = rightFace;
}

void Edge::setLeftPrevEdge(Edge* leftPrevEdge) {
    this->leftPrevEdge = leftPrevEdge;
}

void Edge::setLeftNextEdge(Edge* leftNextEdge) {
    this->leftNextEdge = leftNextEdge;
}

void Edge::setRightPrevEdge(Edge* rightPrevEdge) {
    this->rightPrevEdge = rightPrevEdge;
}

void Edge::setRightNextEdge(Edge* rightNextEdge) {
    this->rightNextEdge = rightNextEdge;
}

std::ostream& operator<<(std::ostream& stream, const Edge& edge) {
    stream << "["
            << "EdgeName: " << edge.getEdgeName()
            << ", SV: " << edge.getStartVertex()->getVertexName()
            << ", EV: " << edge.getEndVertex()->getVertexName()
            << ", LF: " << edge.getLeftFace()->getFaceName()
            << ", RF: " << edge.getRightFace()->getFaceName()
            << ", LP: " << edge.getLeftPrevEdge()->getEdgeName()
            << ", LN: " << edge.getLeftNextEdge()->getEdgeName()
            << ", RP: " << edge.getRightPrevEdge()->getEdgeName()
            << ", RN: " << edge.getRightNextEdge()->getEdgeName()
            << "]";
    return stream;
}