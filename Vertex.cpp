#include "Vertex.h"

Vertex::Vertex(const std::string& vertexName) {
    this->setVertexName(vertexName);
}

const std::string& Vertex::getVertexName() const {
    return this->vertexName;
}

Edge* const Vertex::getIncidentEdge() const {
    return this->incidentEdge;
}

void Vertex::setVertexName(const std::string& vertexName) {
    this->vertexName = vertexName;
}

void Vertex::setIncidentEdge(Edge* incidentEdge) {
    this->incidentEdge = incidentEdge;
}

std::ostream& operator<<(std::ostream& stream, const Vertex& vertex) {
    stream << "["
            << "VertexName: " << vertex.getVertexName()
            << ", IncidentEdge: " << vertex.getIncidentEdge()->getEdgeName()
            << "]";
    return stream;
}