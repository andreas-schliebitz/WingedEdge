#include <iostream>
#include "WingedEdgeTestSuite.h"

void WingedEdgeTestSuite::runAllTests() const {
    this->runEdgeTests();
    this->runFaceTests();
    this->runVertexTests();
}

// Test adjacencies of all edges.
void WingedEdgeTestSuite::runEdgeTests() const {
    std::cout << "=============== Adjacencies of all edges ===============" << std::endl;
    const auto& edgeTable = this->wingedEdge.getEdgeTable();
    for (const auto& edge : edgeTable) {
        const auto& edgeName = edge.first;
        
        std::cout << "getVerticesAdjacentToEdge(\"" << edgeName << "\"):" << std::endl;
        const auto& resVertices = this->wingedEdge.getVerticesAdjacentToEdge(edgeName);
        WingedEdge::printAdjacencies(resVertices);
        
        std::cout << "getEdgesAdjacentToEdge(\"" << edgeName << "\"):" << std::endl;
        const auto& resEdges = this->wingedEdge.getEdgesAdjacentToEdge(edgeName);
        WingedEdge::printAdjacencies(resEdges);
        
        std::cout << "getFacesAdjacentToEdge(\"" << edgeName << "\"):" << std::endl;
        const auto& resFaces = this->wingedEdge.getFacesAdjacentToEdge(edgeName);
        WingedEdge::printAdjacencies(resFaces);
    }
}

// Test adjacencies of all faces.
void WingedEdgeTestSuite::runFaceTests() const {
    std::cout << "=============== Adjacencies of all faces ===============" << std::endl;
    auto faceTable = this->wingedEdge.getFaceTable();
    for (const auto& face : faceTable) {
        const auto& faceName = face.first;
        
        std::cout << "getVerticesAdjacentToFace(\"" << faceName << "\"):" << std::endl;
        const auto& resVertices = this->wingedEdge.getVerticesAdjacentToFace(faceName);
        WingedEdge::printAdjacencies(resVertices);
        
        std::cout << "getEdgesAdjacentToFace(\"" << faceName << "\"):" << std::endl;
        const auto& resEdges = this->wingedEdge.getEdgesAdjacentToFace(faceName);
        WingedEdge::printAdjacencies(resEdges);
        
        std::cout << "getFacesAdjacentToFace(\"" << faceName << "\"):" << std::endl;
        const auto& resFaces = this->wingedEdge.getFacesAdjacentToFace(faceName);
        WingedEdge::printAdjacencies(resFaces);
    }
}

// Test adjacencies of all vertices.
void WingedEdgeTestSuite::runVertexTests() const {
    std::cout << "=============== Adjacencies of all vertices ===============" << std::endl;
    const auto& vertexTable = this->wingedEdge.getVertexTable();
    for (const auto& vertex : vertexTable) {
        const auto& vertexName = vertex.first;
        
        std::cout << "getVerticesAdjacentToVertex(\"" << vertexName << "\"):" << std::endl;
        const auto& resVertices = this->wingedEdge.getVerticesAdjacentToVertex(vertexName);
        WingedEdge::printAdjacencies(resVertices);
        
        std::cout << "getEdgesAdjacentToVertex(\"" << vertexName << "\"):" << std::endl;
        const auto& resEdges = this->wingedEdge.getEdgesAdjacentToVertex(vertexName);
        WingedEdge::printAdjacencies(resEdges);
        
        std::cout << "getFacesAdjacentToVertex(\"" << vertexName << "\"):" << std::endl;
        const auto& resFaces = this->wingedEdge.getFacesAdjacentToVertex(vertexName);
        WingedEdge::printAdjacencies(resFaces);
    }
}