#ifndef POLYGONMODELPARSER_H
#define POLYGONMODELPARSER_H

#include <string>
#include <unordered_map>

#include "json.hpp"
#include "Edge.h"
#include "Vertex.h"
#include  "Face.h"

using json = nlohmann::json;

class PolygonModelParser {
public:
    PolygonModelParser(const std::string& modelFilePath);

    virtual ~PolygonModelParser() {};
    
    void build();
    void parseModelFile(const std::string& path);
    
    void createGeometries();
    void createEdges();
    void createVertices();
    void createFaces();
    
    void buildTables();
    void buildEdgeTable();
    void buildVertexTable();
    void buildFaceTable();

    std::unordered_map<std::string, Edge*> getEdgeTable() const;
    std::unordered_map<std::string, Vertex*> getVertexTable() const;
    std::unordered_map<std::string, Face*> getFaceTable() const;

    static bool fileExists(const std::string& path);
    static const std::string readModelFile(const std::string& path);
private:
    json modelJson;
    std::unordered_map<std::string, Edge*> edgeTable;
    std::unordered_map<std::string, Vertex*> vertexTable;
    std::unordered_map<std::string, Face*> faceTable;
};

#endif /* POLYGONMODELPARSER_H */