#include <fstream>
#include <stdexcept>
#include <iostream>

#include "PolygonModelParser.h"

PolygonModelParser::PolygonModelParser(const std::string& modelFilePath) {
    PolygonModelParser::parseModelFile(modelFilePath);
    this->build();
}

void PolygonModelParser::build() {
    this->createGeometries();
    this->buildTables();
}

void PolygonModelParser::createGeometries() {
    this->createEdges();
    this->createVertices();
    this->createFaces();
}

void PolygonModelParser::buildTables() {
    this->buildEdgeTable();
    this->buildVertexTable();
    this->buildFaceTable();
}

std::unordered_map<std::string, Edge*> PolygonModelParser::getEdgeTable() const {
    return std::move(this->edgeTable);
}

std::unordered_map<std::string, Vertex*> PolygonModelParser::getVertexTable() const {
    return std::move(this->vertexTable);
}

std::unordered_map<std::string, Face*> PolygonModelParser::getFaceTable() const {
    return std::move(this->faceTable);
}

void PolygonModelParser::createEdges() {
    const json& edgeTableJson = this->modelJson["edgeTable"];
    for (const auto& e : edgeTableJson) {
        const std::string& edgeName = e["edgeName"];
        this->edgeTable.insert({edgeName, new Edge(edgeName)});
    }
}

void PolygonModelParser::createVertices() {
    const json& vertexTableJson = this->modelJson["vertexTable"];
    for (const auto& v : vertexTableJson) {
        const std::string& vertexName = v["vertexName"];
        this->vertexTable.insert({vertexName, new Vertex(vertexName)});
    }
}

void PolygonModelParser::createFaces() {
    const json& faceTableJson = this->modelJson["faceTable"];
    for (const auto& f : faceTableJson) {
        const std::string& faceName = f["faceName"];
        this->faceTable.insert({faceName, new Face(faceName)});
    }
}

void PolygonModelParser::buildEdgeTable() {
    const json& edgeTableJson = this->modelJson["edgeTable"];
    
    for (const auto& e : edgeTableJson) {
        Edge* const edge = this->edgeTable.at(e["edgeName"]);
        
        edge->setStartVertex(this->vertexTable.at(e["startVertex"]));
        edge->setEndVertex(this->vertexTable.at(e["endVertex"]));
        
        edge->setLeftFace(this->faceTable.at(e["leftFace"]));
        edge->setRightFace(this->faceTable.at(e["rightFace"]));
        
        edge->setLeftPrevEdge(this->edgeTable.at(e["leftPrevEdge"]));
        edge->setLeftNextEdge(this->edgeTable.at(e["leftNextEdge"]));
        edge->setRightPrevEdge(this->edgeTable.at(e["rightPrevEdge"]));
        edge->setRightNextEdge(this->edgeTable.at(e["rightNextEdge"]));
    }
}

void PolygonModelParser::buildVertexTable() {
    const json& vertexTableJson = this->modelJson["vertexTable"];
    for (const auto& v : vertexTableJson) {
        Vertex* const vertex = this->vertexTable.at(v["vertexName"]);
        vertex->setIncidentEdge(this->edgeTable.at(v["incidentEdge"]));
    }
}

void PolygonModelParser::buildFaceTable() {
    const json& faceTableJson = this->modelJson["faceTable"];
    for (const auto& f : faceTableJson) {
        Face* const face = this->faceTable.at(f["faceName"]);
        face->setIncidentEdge(this->edgeTable.at(f["incidentEdge"]));
    }
}

const std::string PolygonModelParser::readModelFile(const std::string& path) {
    if (fileExists(path)) {
        const std::ifstream in(path);
        std::stringstream json;
        json << in.rdbuf();
        return json.str();
    } else {
        throw std::runtime_error("Polygon model file \"" + path + "\" not found.");
    }
}

void PolygonModelParser::parseModelFile(const std::string& path) {
    this->modelJson = json::parse(PolygonModelParser::readModelFile(path));
}

bool PolygonModelParser::fileExists(const std::string& path) {
    const std::ifstream f(path.c_str());
    return f.good();
}