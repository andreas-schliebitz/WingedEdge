#ifndef FACE_H
#define FACE_H

#include <vector>
#include <string>
#include <ostream>

#include "Edge.h"

class Edge;

class Face {
public:
    Face(const std::string& faceName);
    virtual ~Face() {};

    const std::string& getFaceName() const;
    Edge* const getIncidentEdge() const;

    void setFaceName(const std::string& faceName);
    void setIncidentEdge(Edge* incidentEdge);
private:
    std::string faceName;
    Edge* incidentEdge;
    friend std::ostream& operator<<(std::ostream& stream, const Face& face);
};

#endif /* FACE_H */