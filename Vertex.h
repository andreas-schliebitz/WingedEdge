#ifndef VERTEX_H
#define VERTEX_H

#include <vector>
#include <string>
#include <ostream>

#include "Edge.h"

class Edge;

class Vertex {
public:
    Vertex(const std::string& vertexName);
    virtual ~Vertex() {};
    
    const std::string& getVertexName() const;
    Edge* const getIncidentEdge() const;

    void setVertexName(const std::string& vertexName);
    void setIncidentEdge(Edge* incidentEdge);
private:
    std::string vertexName;
    Edge* incidentEdge;
    friend std::ostream& operator<<(std::ostream& stream, const Vertex& vertex);
};

#endif /* VERTEX_H */