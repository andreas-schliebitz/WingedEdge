#ifndef EDGE_H
#define EDGE_H

#include <string>
#include <ostream>

#include "Face.h"
#include "Vertex.h"

class Face;
class Vertex;

class Edge {
public:
    Edge(const std::string& edgeName);

    virtual ~Edge() {};

    const std::string& getEdgeName() const;
    Vertex* const getStartVertex() const;
    Vertex* const getEndVertex() const;
    Face* const getLeftFace() const;
    Face* const getRightFace() const;
    Edge* const getLeftPrevEdge() const;
    Edge* const getLeftNextEdge() const;
    Edge* const getRightPrevEdge() const;
    Edge* const getRightNextEdge() const;

    void setEdgeName(const std::string& edgeName);
    void setStartVertex(Vertex* startVertex);
    void setEndVertex(Vertex* endVertex);
    void setLeftFace(Face* leftFace);
    void setRightFace(Face* rightFace);
    void setLeftPrevEdge(Edge* leftPrevEdge);
    void setLeftNextEdge(Edge* leftNextEdge);
    void setRightPrevEdge(Edge* rightPrevEdge);
    void setRightNextEdge(Edge* rightNextEdge);
private:
    std::string edgeName;
    Vertex* startVertex, *endVertex;
    Face* leftFace, *rightFace;
    Edge* leftPrevEdge, *leftNextEdge, *rightPrevEdge, *rightNextEdge;
    friend std::ostream& operator<<(std::ostream& stream, const Edge& edge);
};

#endif /* EDGE_H */