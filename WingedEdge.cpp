#include <iostream>

#include "WingedEdge.h"
#include "PolygonModelParser.h"

WingedEdge::WingedEdge(const std::string& modelPath) {
    PolygonModelParser parser(modelPath);
    
    this->edgeTable = parser.getEdgeTable();
    this->vertexTable = parser.getVertexTable();
    this->faceTable = parser.getFaceTable();
}

WingedEdge::~WingedEdge() {
    std::cout << "Freeing allocated heap memory..." << std::endl;
    for (auto& e : this->edgeTable) {
        delete e.second; 
        e.second = nullptr;
    }
    
    for (auto& v : this->vertexTable) {
        delete v.second;
        v.second = nullptr;
    }
    
    for (auto& f : this->faceTable) {
        delete f.second;
        f.second = nullptr;
    }
    std::cout << "Done." << std::endl;
}

const std::unordered_map<std::string, Vertex*>& WingedEdge::getVertexTable() const {
    return this->vertexTable;
}

const std::unordered_map<std::string, Edge*>& WingedEdge::getEdgeTable() const {
    return this->edgeTable;
}

const std::unordered_map<std::string, Face*>& WingedEdge::getFaceTable() const {
    return this->faceTable;
}

// ##### Adjacencies of edge #####

// Kante -> Eckpunkte
std::vector<Vertex*> WingedEdge::getVerticesAdjacentToEdge(const std::string& edgeName) const {
    const Edge* const edge = this->edgeTable.at(edgeName);
    return {edge->getStartVertex(), edge->getEndVertex()};
}

// Kante -> benachbarte Kanten
std::vector<Edge*> WingedEdge::getEdgesAdjacentToEdge(const std::string& edgeName) const {  
    const Edge* const startEdge = this->edgeTable.at(edgeName);
    const auto& startEndVertices = {startEdge->getStartVertex(), startEdge->getEndVertex()};
    
    std::vector<Edge*> edges;
    for (const Vertex* const vertex : startEndVertices) {
        const auto& adjEdges = this->getEdgesAdjacentToVertex(vertex->getVertexName());
        edges.insert(edges.end(), adjEdges.begin(), adjEdges.end());
    }
    
    edges.erase(std::remove(edges.begin(), edges.end(), startEdge), edges.end());
    return edges;
}

// Kante -> Facetten
std::vector<Face*> WingedEdge::getFacesAdjacentToEdge(const std::string& edgeName) const {
    const Edge* const edge = this->edgeTable.at(edgeName);
    return {edge->getLeftFace(), edge->getRightFace()};
}

// ##### Adjacencies of vertex #####

// Eckpunkt -> benachbarte Eckpunkte
std::vector<Vertex*> WingedEdge::getVerticesAdjacentToVertex(const std::string& vertexName) const {
    const Vertex* const vertex = this->vertexTable.at(vertexName);
    const Edge* curEdge = vertex->getIncidentEdge();
    const Edge* const startEdge = curEdge;

    std::vector<Vertex*> vertices;
    do {
        Vertex* const startVertex = curEdge->getStartVertex();
        if (startVertex == vertex) {
            curEdge = curEdge->getLeftNextEdge();
            vertices.push_back(curEdge->getEndVertex());
        } else {
            vertices.push_back(startVertex);
            curEdge = curEdge->getRightNextEdge();
        }
    } while (curEdge != startEdge);

    return vertices;
}

// Eckpunkt -> Kanten
std::vector<Edge*> WingedEdge::getEdgesAdjacentToVertex(const std::string& vertexName) const {
    const Vertex* const vertex = this->vertexTable.at(vertexName);
    Edge* curEdge = vertex->getIncidentEdge();
    const Edge* const startEdge = curEdge;
    
    std::vector<Edge*> edges;
    do {
        edges.push_back(curEdge);
        if (curEdge->getStartVertex() == vertex) {
            curEdge = curEdge->getRightPrevEdge();
        } else {
            curEdge = curEdge->getLeftPrevEdge();
        }
    } while (curEdge != startEdge);
    
    return edges;
}

// Eckpunkt -> Facetten
std::unordered_set<Face*> WingedEdge::getFacesAdjacentToVertex(const std::string& vertexName) const {
    const auto& adjEdges = this->getEdgesAdjacentToVertex(vertexName);
    
    std::unordered_set<Face*> faces;
    for (const Edge* const edge : adjEdges) {
        faces.insert(edge->getLeftFace());
        faces.insert(edge->getRightFace());
    }

    return faces;
}

// ##### Adjacencies of face #####

// Facette -> Eckpunkte
std::unordered_set<Vertex*> WingedEdge::getVerticesAdjacentToFace(const std::string& faceName) const {
    const Face* const face = this->faceTable.at(faceName);
    const auto& adjEdges = this->getEdgesAdjacentToFace(face->getFaceName());
    
    std::unordered_set<Vertex*> vertices;
    for (const Edge* const edge : adjEdges) {
        vertices.insert(edge->getStartVertex());
        vertices.insert(edge->getEndVertex());
    }
    
    return vertices;
}

// Facette -> Kanten
std::vector<Edge*> WingedEdge::getEdgesAdjacentToFace(const std::string& faceName) const {
    const Face* const face = this->faceTable.at(faceName);
    Edge* curEdge = face->getIncidentEdge();
    const Edge* startEdge = curEdge;

    std::vector<Edge*> edges;
    do {
        edges.push_back(curEdge);
        if (curEdge->getLeftFace() == face) {
            curEdge = curEdge->getLeftNextEdge();
        } else {
            curEdge = curEdge->getRightNextEdge();
        }
    } while (curEdge != startEdge);

    return edges;
}

// Facette -> benachbarte Facetten
std::unordered_set<Face*> WingedEdge::getFacesAdjacentToFace(const std::string& faceName) const {
    const Face* const face = this->faceTable.at(faceName);
    const auto& adjEdges = this->getEdgesAdjacentToFace(faceName);
    
    std::unordered_set<Face*> faces;
    for (const Edge* const edge : adjEdges) {
        Face* const leftFace = edge->getLeftFace();
        if (leftFace != face) faces.insert(leftFace);
        
        Face* const rightFace = edge->getRightFace();
        if (rightFace != face) faces.insert(rightFace);
    }

    return faces;
}